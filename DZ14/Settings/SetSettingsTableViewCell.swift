//
//  SetSettingsTableViewCell.swift
//  DZ14
//
//  Created by Mahutin Aleksei on 15/09/2019.
//  Copyright © 2019 Mahutin Aleksei. All rights reserved.
//

import UIKit

class SetSettingsTableViewCell: UITableViewCell {

    @IBOutlet weak var settings: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        
        // Configure the view for the selected state
    }

}
