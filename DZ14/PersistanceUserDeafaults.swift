//
//  Persistance.swift
//  DZ14
//
//  Created by Mahutin Aleksei on 13/09/2019.
//  Copyright © 2019 Mahutin Aleksei. All rights reserved.
//

import Foundation

class PersistanceUserDeafaults{
    static let shared = PersistanceUserDeafaults()
    
    private let kUserNameKey = "PersistanceUserDeafaults.userName"
    
    var userName: String? {
        set{UserDefaults.standard.set(newValue, forKey: kUserNameKey)}
        get{ return UserDefaults.standard.string(forKey: kUserNameKey)}
    }
    
    private let kUserLastNameKey = "PersistanceUserDeafaults.userLastName"
    
    var userLastName: String? {
        set{UserDefaults.standard.set(newValue, forKey: kUserLastNameKey)}
        get{ return UserDefaults.standard.string(forKey: kUserLastNameKey)}
    }
    
    private let kUserDefaultCityKey = "PersistanceUserDeafaults.userDefaultCIty"
    
    var userDefaultCIty: String? {
        set{UserDefaults.standard.set(newValue, forKey: kUserDefaultCityKey)}
        get{ return UserDefaults.standard.string(forKey: kUserDefaultCityKey)}
    }
    
    private let kUserDefaultUnitKey = "PersistanceUserDeafaults.userDefaultUnit"
    
    var userDefaultUnit: String? {
        set{UserDefaults.standard.set(newValue, forKey: kUserDefaultUnitKey)}
        get{ return UserDefaults.standard.string(forKey: kUserDefaultUnitKey)}
    }
    
    func saveSettings(settings:String,value:String) {
        switch settings {
        case "City":
            userDefaultCIty = value
        case "Unit":
            userDefaultUnit = value
        default:
            break
        }
    }
}
