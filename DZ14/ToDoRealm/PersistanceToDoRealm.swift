//
//  File.swift
//  DZ14
//
//  Created by Mahutin Aleksei on 15/09/2019.
//  Copyright © 2019 Mahutin Aleksei. All rights reserved.
//

import Foundation
import RealmSwift

class ToDoRealm:Object {
    @objc dynamic var work = ""
    @objc dynamic var isComplete = false
    @objc dynamic var date:Double = 0
}

class PersistanceToDoRealm {
    static let shared = PersistanceToDoRealm()
    
    private let realm = try! Realm()
    
    func saveToDoRalm(todo:ToDoRealm) {
        try! realm.write {
            realm.add(todo)
        }
    }
    
    func loadToDoRealm() -> [ToDoRealm] {
        var result = [ToDoRealm]()
        let data = realm.objects(ToDoRealm.self).elements
        for todo in data {
            result.append(todo)
        }
        
        result = result.sorted(by: { todo,todo2 in
            todo.date < todo2.date
        })
        return result
    }

    func saveAllTodo(data:[ToDoRealm]) {
        for todo in data    {
            saveToDoRalm(todo: todo)
        }
    }
    
    func clearAllTodo() {
        let data = realm.objects(ToDoRealm.self).elements
        for todo in data {
            try! realm.write {
                realm.delete(todo)
            }
        }
    }
    
    func delTodo(element:ToDoRealm) {
        let oldValue = realm.objects(ToDoRealm.self).filter("date == \(element.date)")
        try! realm.write {
            realm.delete(oldValue)
        }
    }
    
    func changeIsComplite(element:ToDoRealm,isComplite:Bool) {
        let oldValue = realm.objects(ToDoRealm.self).filter("date == \(element.date)").first
        if let old = oldValue {
            try! realm.write {
                realm.delete(old)
            }
        }
        element.isComplete = isComplite
        try! realm.write {
            realm.add(element)
        }
    }
    
}
