//
//  ToDo.swift
//  DZ14
//
//  Created by Mahutin Aleksei on 15/09/2019.
//  Copyright © 2019 Mahutin Aleksei. All rights reserved.
//

import Foundation

struct ToDo {
    var description: String
    var isComplete: Bool
}
