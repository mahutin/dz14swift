//
//  File.swift
//  DZ14
//
//  Created by Mahutin Aleksei on 15/09/2019.
//  Copyright © 2019 Mahutin Aleksei. All rights reserved.
//
import UIKit
import CoreData

//class ToDoRealm:Object {
//    @objc dynamic var work = ""
//    @objc dynamic var isComplete = false
//    @objc dynamic var date:Double = 0
//}
//
class PersistanceToCoreData {
    static let shared = PersistanceToCoreData()
    
    var todoDates = [NSManagedObject]()
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    
    func saveToDo(todoNew:ToDoRealm) {
        let context = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "ToDoCoreData", in: context)
        let todo = NSManagedObject(entity: entity!, insertInto: context)
        todo.setValue(todoNew.work, forKey: "work")
        todo.setValue(todoNew.date, forKey: "date")
        todo.setValue(todoNew.isComplete, forKey: "isComplete")
        try? context.save()
    }
    
    func loadToDo() -> [ToDoRealm] {
        var answear = [ToDoRealm]()
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "ToDoCoreData")
        //request.predicate = NSPredicate(format: "age = %@", "12")
        request.returnsObjectsAsFaults = false
        let result = try? context.fetch(request)
        for data in result as! [NSManagedObject] {
            let todo = ToDoRealm()
            
            todo.date = data.value(forKey: "date") as! Double
            todo.isComplete = data.value(forKey: "isComplete") as! Bool
            todo.work = data.value(forKey: "work") as! String
            print(todo.date)
            answear.append(todo)
        }
        return answear.sorted(by:{ todo,todo2 in
            todo.date < todo2.date
        })
        
    }
    
    func clearAllTodo() {
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "ToDoCoreData")
        request.returnsObjectsAsFaults = false
        let result = try! context.fetch(request)
        for object in result{
            context.delete(object as! NSManagedObject)
        }
    }
    
    func delTodo(element:ToDoRealm) {
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "ToDoCoreData")
        request.predicate = NSPredicate.init(format: "date==\(element.date)")
        if let result = try? context.fetch(request) {
            for object in result {
                context.delete(object as! NSManagedObject)
            }
        }
    }
    
    func changeIsComplite(element:ToDoRealm,isComplite:Bool) {
        let data = element
        data.isComplete = isComplite
        delTodo(element: element)
        saveToDo(todoNew: data)
    }
    
}



//    func changeIsComplite(element:ToDoRealm,isComplite:Bool) {
//        let oldValue = realm.objects(ToDoRealm.self).filter("date == \(element.date)").first
//        if let old = oldValue {
//            try! realm.write {
//                realm.delete(old)
//            }
//        }
//        element.isComplete = isComplite
//        try! realm.write {
//            realm.add(element)
//        }
//    }
//
//}
