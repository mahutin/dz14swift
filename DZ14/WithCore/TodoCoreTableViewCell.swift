//
//  TodoCoreTableViewCell.swift
//  DZ14
//
//  Created by Mahutin Aleksei on 15/09/2019.
//  Copyright © 2019 Mahutin Aleksei. All rights reserved.
//

import UIKit

class TodoCoreTableViewCell: UITableViewCell {

    @IBOutlet weak var work: UILabel!
    @IBOutlet weak var data: UILabel!
    @IBOutlet weak var isComplete: UISwitch!
    var date:Double = 0.0
    
    
    @IBAction func isCompleteChange(_ sender: Any) {
        let todo = ToDoRealm()
        todo.date = date
        todo.work = work.text ?? ""
        todo.isComplete = isComplete.isOn
        PersistanceToCoreData.shared.changeIsComplite(element: todo,isComplite: isComplete.isOn)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        // Configure the view for the selected state
    }
    
}
