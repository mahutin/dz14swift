//
//  CoreWithCoreDataTableViewController.swift
//  DZ14
//
//  Created by Mahutin Aleksei on 15/09/2019.
//  Copyright © 2019 Mahutin Aleksei. All rights reserved.
//

import UIKit

class CoreWithCoreDataTableViewController: UITableViewController {

    @IBOutlet weak var workTextField: UITextField!
    
    @IBOutlet weak var delAllButton: UIButton!
    
    private var todoData:[ToDoRealm] = PersistanceToCoreData.shared.loadToDo()
    
    @IBAction func keyboardHide(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    @IBAction func addButtonPress(_ sender: Any) {
        if let text = workTextField.text, text.count > 3 {
            let work = ToDoRealm()
            work.date = NSDate().nowDouble()
            work.isComplete = false
            work.work = text
            PersistanceToCoreData.shared.saveToDo(todoNew: work)
            todoData = PersistanceToCoreData.shared.loadToDo()
            updateTable()
            workTextField.text = ""
        }
    }
    
    @IBAction func delAllButtonPress(_ sender: Any) {
        todoData = []
        PersistanceToCoreData.shared.clearAllTodo()
        updateTable()
    }
    
    private func updateTable() {
        tableView.reloadData()
        if todoData.count == 0 {
            UIView.animate(withDuration: 0.5, animations: {
                self.delAllButton.layer.opacity = 0
            })
        }else{
            UIView.animate(withDuration: 0.5, animations: {
                self.delAllButton.layer.opacity = 1
            })
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath:
        IndexPath) -> CGFloat {
        
        return 80
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    

    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return todoData.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "todoher2") as! TodoCoreTableViewCell
        let todo = todoData[indexPath.row]
        var date = NSDate(timeIntervalSince1970: todo.date ).description.split(separator: " ")
        date.removeLast()
        cell.data.text = date.joined(separator: " ")
        cell.date = todo.date
        cell.isComplete.isOn = todo.isComplete
        cell.work.text = todo.work
        // Configure the cell...
        return cell
    }
    
    override func tableView(_ tableView: UITableView,
                            trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
    {
        // Write action code for the trash
        let deleteAction = UIContextualAction(style: .normal, title:  "Delete", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            PersistanceToCoreData.shared.delTodo(element: self.todoData[indexPath[1]])
            self.todoData = PersistanceToCoreData.shared.loadToDo()
            self.updateTable()
            success(true)
        })
        deleteAction.backgroundColor = .red
        
        return UISwipeActionsConfiguration(actions: [deleteAction])
    }
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
