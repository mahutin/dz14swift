//
//  WeatherDayCell.swift
//  DZ13
//
//  Created by Mahutin Aleksei on 10/09/2019.
//  Copyright © 2019 Mahutin Aleksei. All rights reserved.
//

import UIKit

class WeatherDayCell: UITableViewCell {

    @IBOutlet weak var weatherImage: UIImageView!
    @IBOutlet weak var descriptionWeather: UILabel!
    @IBOutlet weak var temperature: UILabel!
    @IBOutlet weak var dataOrTime: UILabel!
    var index:Int = 0
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
            }
}
