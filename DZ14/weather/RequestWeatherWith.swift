//
//  RequestWeatherWith.swift
//  DZ13
//
//  Created by Mahutin Aleksei on 11/09/2019.
//  Copyright © 2019 Mahutin Aleksei. All rights reserved.
//

import Foundation
import Alamofire

class RequestWeatherWith {
    var city = PersistanceUserDeafaults.shared.userDefaultCIty ?? "Moscow"
    var units = PersistanceUserDeafaults.shared.userDefaultUnit ?? "metric"
    
    private func updateVariable() {
        city = PersistanceUserDeafaults.shared.userDefaultCIty ?? "Moscow"
        units = PersistanceUserDeafaults.shared.userDefaultUnit ?? "metric"
    }
    
    /**
     Load data weather for one day, then return Weather in Callback
     
     Load data from with weather from openweathermap api,then return this data in callback void.
     This function send request witch framework as Alamofire
     
     :param:  complete: void function.
     */
    func loadCurrentWeather (complete:@escaping (_ weather:Weather) -> Void){
        updateVariable()
        let url = URL(string: "https://api.openweathermap.org/data/2.5/weather?q=\(city)&APPID=009320b522a39d8c08e509b26be88d4e&units=\(units)")!
        Alamofire.request(url).validate().responseJSON(completionHandler: {
            response in
            guard let jsonDict = response.result.value as? NSDictionary,
                    let main = jsonDict["main"] as? NSDictionary,
                    let weatherArr = jsonDict["weather"] as? NSArray,
                    let weatherDict = weatherArr[0] as? NSDictionary,
                    let description = weatherDict["description"] as? String,
                    let dt = jsonDict["dt"] as? Double,
                    let currentTemp = main["temp"] as? Double else {
                        complete(Weather(0, description: "error with server", time: 1))
                        return}
                let weather = Weather(currentTemp, description: description,time: dt)
                DispatchQueue.main.async {
                    complete(weather)
                }
        })
    }
    
    /**
     Load data weather for five days, then return [Weather] in Callback
     
     Load data from with weather from openweathermap api,then return this data in callback void.
     This function send request witch framework as Alamofire
     
     :param:  complete: void function.
     */
    func loadFiveCurrentWeather (complete:@escaping (_ days:[String:[Weather]]) -> Void){
        updateVariable()
        let url = URL(string: "https://api.openweathermap.org/data/2.5/forecast?q=\(city)&APPID=009320b522a39d8c08e509b26be88d4e&units=\(units)")!
        Alamofire.request(url).responseJSON(completionHandler: {
            response in
            guard let jsonDict = response.value as? NSDictionary,let list = jsonDict["list"] as? NSArray else {
                let data:[String:[Weather]] = [String:[Weather]]()
                complete(data)
                return
                
            }
                var days:[String:[Weather]] = [:]
                for eleme in list {
                    guard let day = eleme as? NSDictionary,
                        let main = day["main"] as? NSDictionary,
                        let weatherArr = day["weather"] as? NSArray,
                        let dt = day["dt"] as? Double,
                        let weatherDict = weatherArr[0] as? NSDictionary,
                        let description = weatherDict["description"] as? String,
                        let currentTemp = main["temp"] as? Double else {continue}
                    let data = NSDate(timeIntervalSince1970: dt)
                    if days["\(data.description.split(separator: " ")[0])"] != nil {} else{ days["\(data.description.split(separator: " ")[0])"] = [Weather]() }
                    days["\(data.description.split(separator: " ")[0])"]?.append(Weather(currentTemp, description: description,time: dt))
                }
                DispatchQueue.main.async {
                    complete(days)
                }
        })
    }
    
    func changeUnits() {
        switch units {
        case "metric":
            units = "imperial"
        case "imperial":
            units = "metric"
        default:
            units = "metric"
        }
    }
}
