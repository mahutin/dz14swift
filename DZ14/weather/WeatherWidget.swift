//
//  WeatherWidget.swift
//  DZ13
//
//  Created by Mahutin Aleksei on 09/09/2019.
//  Copyright © 2019 Mahutin Aleksei. All rights reserved.
//

import UIKit

@IBDesignable
class WeatherWidget: UIView {
    
    private var isSetup = false
    
    private let weatherImage = UIImageView()
    private let city = UILabel()
    private let weather = UILabel()
    private let temperature = UILabel()
    private let view = UIView()

    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        //serup view
        view.frame.size = CGSize(width: self.frame.width, height: self.frame.height-10)
        view.backgroundColor = UIColor.blue.withAlphaComponent(0.02)
        view.layer.cornerRadius = view.frame.width / 8
        
        //setup other views
        weatherImage.contentMode = .scaleAspectFit
        
        city.textAlignment = .center
        city.text = PersistanceUserDeafaults.shared.userDefaultCIty ?? "Moscow"
        city.font = UIFont(name: "HelveticaNeue-Light", size: 24)
        
        weather.textAlignment = .center
        weather.text = "Error"
        weather.font = UIFont(name: "HelveticaNeue-Light", size: 20)
        
        temperature.textAlignment = .center
        temperature.text = "0"
        temperature.font = UIFont(name: "HelveticaNeue-Light", size: 80)

        
        if (isSetup) { return }
        addSubview(view)
        addSubview(weatherImage)
        addSubview(city)
        addSubview(temperature)
        addSubview(weather)
        
        setTextConstraint()
        
        isSetup = true
    }
    
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        setTextConstraint()
    }
    
    /**
     create constrains for this widget
    */
    private func setTextConstraint() {
        weatherImage.translatesAutoresizingMaskIntoConstraints = false
        weatherImage.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 50).isActive = true
        weatherImage.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -50).isActive = true
        weatherImage.topAnchor.constraint(equalTo: city.bottomAnchor,constant: 10).isActive = true
       
        
        for text in [city,weather,temperature]{
            text.translatesAutoresizingMaskIntoConstraints = false
            text.leadingAnchor.constraint(equalTo: view.leadingAnchor , constant: 0).isActive = true
            text.trailingAnchor.constraint(equalTo: view.trailingAnchor , constant: 0).isActive = true
        }
        city.topAnchor.constraint(equalTo: view.topAnchor, constant: 10).isActive = true
        temperature.bottomAnchor.constraint(equalTo: view.bottomAnchor,constant: -10).isActive = true
        weather.bottomAnchor.constraint(equalTo: temperature.topAnchor, constant: 0).isActive
            = true
        weatherImage.bottomAnchor.constraint(equalTo: weather.topAnchor).isActive = true
    }
    
    /**
     update widget
    */
    func update(weather:Weather) {
        city.text = PersistanceUserDeafaults.shared.userDefaultCIty ?? "Moscow"
        weatherImage.image = weather.img
        self.weather.text = weather.description
        self.temperature.text = "\(weather.temperature)°"
        layoutIfNeeded()
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
