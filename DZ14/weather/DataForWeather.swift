//
//  DataForWeather.swift
//  DZ14
//
//  Created by Mahutin Aleksei on 14/09/2019.
//  Copyright © 2019 Mahutin Aleksei. All rights reserved.
//

import Foundation

class DataForWaether {
    static let shared = DataForWaether()
    
    let data = RequestWeatherWith()
    var day: Weather? = PersistanceWeather.shared.loadTodayWeather()
    var days: [[Weather]] = PersistanceWeather.shared.loadAllWeathers()
    var isLoad = false

    func loadWeather(complete:@escaping () -> Void = {}){
        data.loadCurrentWeather(complete: {
            (data)  in
            self.day = data
            PersistanceWeather.shared.saveTodayWeather(today: data)
        })
        data.loadFiveCurrentWeather(complete: {
            (data) in
            var full = [[Weather]]()
            for values in data.values{
                full.append(values)
            }
            self.days = full.sorted(by: {arr,arr2 in
                return arr[0].data.timeIntervalSince1970 < arr2[0].data.timeIntervalSince1970
            })
            PersistanceWeather.shared.saveAllWeathers(allData: full)
            self.isLoad = true
            complete()
        })
        
    }
}
