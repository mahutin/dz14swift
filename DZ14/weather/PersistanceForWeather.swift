//
//  PersistanceForWeather.swift
//  DZ14
//
//  Created by Mahutin Aleksei on 14/09/2019.
//  Copyright © 2019 Mahutin Aleksei. All rights reserved.
//

import Foundation
import RealmSwift


class WeatherRealm: Object {
    @objc dynamic var weatherDescription: String = ""
    @objc dynamic var temperature: Int = 0
    @objc dynamic var date: Int = 0
    @objc dynamic var today:Bool = false
}

class PersistanceWeather {
    static let shared = PersistanceWeather()
    
    private let realm = try! Realm()
    
    func saveTodayWeather(today:Weather) {
        let weather = WeatherRealm()
        weather.date = Int(today.data.timeIntervalSince1970)
        weather.temperature = Int(today.temperature)
        weather.weatherDescription = today.description
        weather.today = true
        //delete old data
        let oldData = realm.objects(WeatherRealm.self).filter("today == true")
        try! realm.write {
            for data in oldData {
                realm.delete(data)
            }
        }
        //save new data
        try! realm.write {
            realm.add(weather)
        }
    }
    
    func saveAllWeathers(allData:[[Weather]]) {
        //delete oldData
        let oldData = realm.objects(WeatherRealm.self).filter("today == false")
        try! realm.write {
            for data in oldData {
                realm.delete(data)
            }
        }
        //create new data
        for temp in allData {
            for data in temp{
                let weather = WeatherRealm()
                weather.date = Int(data.data.timeIntervalSince1970)
                weather.temperature = Int(data.temperature)
                weather.weatherDescription = data.description
                weather.today = false
                try! realm.write {
                    realm.add(weather)
                }
            }
        }
    }
    
    func loadTodayWeather() -> Weather? {
        if let oldData = realm.objects(WeatherRealm.self).filter("today == true").first {
            let weather = Weather(Double(oldData.temperature), description: oldData.weatherDescription, time: Double(oldData.date))
            return weather
        }
        return nil
    }
    
    func loadAllWeathers() -> [[Weather]] {
        //save all weather in temp arr
        var tempArr = [Weather]()
        let allDays = realm.objects(WeatherRealm.self).filter("today == false").elements
        for day in allDays {
            tempArr.append(Weather(Double(day.self.temperature), description: day.self.weatherDescription, time: Double(day.self.date)))
        }
        //sort weather by date day
        var tempDict = [String:[Weather]]()
        for elem in tempArr {
            if tempDict["\(elem.data.description.split(separator: " ").first!)"] != nil {
            } else {
                tempDict["\(elem.data.description.split(separator: " ").first!)"] = [Weather]()
            }
            tempDict["\(elem.data.description.split(separator: " ").first!)"]?.append(elem)
        }
        //spread day arr with temp in arr
        var full = [[Weather]]()
        for values in tempDict.values{
            full.append(values)
        }
        //sort arr by date
        full = full.sorted(by: {arr,arr2 in
            return arr[0].data.timeIntervalSince1970 < arr2[0].data.timeIntervalSince1970
        })
        return full
    }
   
    
    
}
