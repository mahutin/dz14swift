//
//  Weather.swift
//  DZ13
//
//  Created by Mahutin Aleksei on 10/09/2019.
//  Copyright © 2019 Mahutin Aleksei. All rights reserved.
//

import UIKit

struct Weather {
    let temperature:Int
    let description:String
    let img:UIImage
    let data:NSDate
    init(_ currentTemp:Double, description:String,time:Double){
        
        self.data = NSDate(timeIntervalSince1970: time)
        self.temperature = Int(currentTemp)
        switch description {
        case "clear sky":
            self.description = description
            self.img = UIImage(named: "clearSky")!
        case "broken clouds":
            self.description = description
            self.img = UIImage(named: "brokenClouds")!
        case "light rain":
            self.description = description
            self.img = UIImage(named: "lightRain")!
        case "scattered clouds":
            self.description = description
            self.img = UIImage(named: "scatteredClouds")!
        case "overcast clouds":
            self.description = description
            self.img = UIImage(named: "overcastClouds")!
        case "light intensity shower rain":
            self.description = description
            self.img = UIImage(named: "lightRain")!
        default:
            self.description = description
            self.img = UIImage(named: "scatteredClouds")!
        }
        
    }
}
