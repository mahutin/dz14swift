//
//  WithViewController.swift
//  DZ13
//
//  Created by Mahutin Aleksei on 11/09/2019.
//  Copyright © 2019 Mahutin Aleksei. All rights reserved.
//

import UIKit

class WithViewController: UIViewController {

    @IBOutlet weak var tableVIew: UITableView!
    @IBOutlet weak var mainWidget: WeatherWidget!
    
    private var timeOutUpdateWeather = 0
    private var maxTimeOutCount = 30
    
    private var days:[[Weather]] = []
    private var day:Weather?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableVIew.layer.opacity = 0
    }
    
    private func updateWeatherRows(){
        //update variable
        days = DataForWaether.shared.days
        day = DataForWaether.shared.day
        //update tableview and mainWidget
        tableVIew.reloadData()
        mainWidget.update(weather: day ?? Weather(0, description: "load", time: 1))
        //show table view
        UIView.animate(withDuration: 0.5, animations: {
            self.tableVIew.layer.opacity = 1
        })
        //if dont new weather load, try load after 1 second
        if DataForWaether.shared.isLoad == false && timeOutUpdateWeather < maxTimeOutCount {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.timeOutUpdateWeather += 1
            self.updateWeatherRows()
        }}
        //if
        if timeOutUpdateWeather == maxTimeOutCount {
            day = Weather(0, description: "Check internet conection", time: 1)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        self.mainWidget.frame.size = CGSize(width: mainWidget.frame.width, height: self.view.bounds.height / 2.2)
        self.tableVIew.layer.cornerRadius = tableVIew.frame.width / 8
        self.tableVIew.layer.maskedCorners = [.layerMaxXMaxYCorner,.layerMinXMaxYCorner]
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        updateWeatherRows()
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? SelectedRowWithWeather, segue.identifier == "showWaetherWih" {
            // send days data to next view controller
            let sendData = days[Int(tableVIew.indexPathsForSelectedRows!.first![1])]
            vc.data = sendData
            vc.tableView.reloadData()
            let date = sendData[0].data.description.split(separator: " ")[0].split(separator: "-")
            vc.dateLabel.text = "\(date[2])-\(date[1])-\(date[0])"
        }
    }
    
}


extension WithViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return days.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //write data in cell propities
        let cell = tableVIew.dequeueReusableCell(withIdentifier: "weather") as! WeatherDayCell
        let weathers = days[indexPath.row]
        let showTempIndex = days[indexPath.row].count > 5 ? 5 : days[indexPath.row].count - 1
        cell.temperature.text = "\(weathers[showTempIndex].temperature)°"
        cell.weatherImage.image = weathers[showTempIndex].img
        cell.descriptionWeather.text = "\(weathers[showTempIndex].description)"
        let date = weathers[showTempIndex].data.description.split(separator: " ")[0].split(separator: "-")
        cell.dataOrTime.text = "\(date[2]).\(date[1])"
        cell.index = indexPath.row
        
        //cell customize
        cell.backgroundColor = UIColor.blue.withAlphaComponent(0.02)
        switch indexPath.row {
        case 0:
            cell.layer.cornerRadius = cell.frame.width / 8
            cell.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        case days.count - 1:
            cell.layer.cornerRadius = cell.frame.width / 8
            cell.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        default:
            break
        }
        
        return cell
    }
}
