//
//  NSdate.swift
//  DZ14
//
//  Created by Mahutin Aleksei on 15/09/2019.
//  Copyright © 2019 Mahutin Aleksei. All rights reserved.
//

import Foundation

extension NSDate {
    
    func nowDouble() -> Double {
        let now = NSDate(timeIntervalSince1970: NSDate().timeIntervalSince1970 + 10800)
        return now.timeIntervalSince1970
    }
}
