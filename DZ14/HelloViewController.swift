//
//  HelloViewController.swift
//  DZ14
//
//  Created by Mahutin Aleksei on 13/09/2019.
//  Copyright © 2019 Mahutin Aleksei. All rights reserved.
//

import UIKit

class HelloViewController: UIViewController {

    @IBOutlet weak var textFieldsForUserInfo: UIView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var goButton: UIButton!
    @IBOutlet weak var helloLabel: UILabel!
    
    @IBAction func nameTextFieldChange(_ sender: Any) {
        let text:String = nameTextField.text ?? ""
        if text.count >= 3 {
            animatedHideOrUnhide(Element: lastNameTextField, Hide: false)
        }else{
            animatedHideOrUnhide(Element: lastNameTextField, Hide: true)
        }
    }
    @IBAction func lastNameTextFieldChange(_ sender: Any) {
        let text:String = lastNameTextField.text ?? ""
        if text.count >= 3 {
            animatedHideOrUnhide(Element: goButton, Hide: false)
        }else{
            animatedHideOrUnhide(Element: goButton, Hide: true)
        }
    }
    
    @IBAction func pressGoButton(_ sender: Any) {
        PersistanceUserDeafaults.shared.userName = nameTextField.text!
        PersistanceUserDeafaults.shared.userLastName = lastNameTextField.text!
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DataForWaether.shared.loadWeather(complete: {})
        if PersistanceUserDeafaults.shared.userName != nil {
            textFieldsForUserInfo.layer.opacity = 0
            let name = PersistanceUserDeafaults.shared.userName!
            let lastName = PersistanceUserDeafaults.shared.userLastName!
            helloLabel.text = "\(lastName) \(name)"
            nameTextField.text = name
            lastNameTextField.text = lastName
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                self.goButton.sendActions(for: .touchUpInside)
            }
        }else{
            helloLabel.layer.opacity = 0
            lastNameTextField.layer.opacity = 0
            goButton.layer.opacity = 0
        }

    }
    
    private func animatedHideOrUnhide(Element:UIView,Hide:Bool){
        UIView.animate(withDuration: 1, animations: {
            if Hide {
                Element.layer.opacity = 0
            }else{
                Element.layer.opacity = 1
            }
            
        })
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
